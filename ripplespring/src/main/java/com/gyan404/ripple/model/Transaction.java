package com.gyan404.ripple.model;

public class Transaction {

    private String targetUser;
    private Amount amount;

    public Transaction(String targetUser, Amount amount) {
        this.targetUser = targetUser;
        this.amount = amount;
    }

    public String getTargetUser() {
        return targetUser;
    }

    public Amount getAmount() {
        return amount;
    }
}
