package com.gyan404.ripple.model;

import java.util.concurrent.atomic.AtomicInteger;

public class Amount {
    private AtomicInteger balance;
    /**
     * Currency currently is only a cosmetic convenience for now. Can be used for currency conversion extensions
     **/
    private String currency;

    public Amount(AtomicInteger balance, String currency) {
        this.balance = balance;
        this.currency = currency;
    }

    public void subtract(int amount) {
        balance.updateAndGet(value -> value - amount);
    }

    public void add(int amount) {
        balance.updateAndGet(value -> value + amount);
    }

    public AtomicInteger getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return currency + balance.get();
    }
}
