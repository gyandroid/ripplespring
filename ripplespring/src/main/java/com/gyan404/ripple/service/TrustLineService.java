package com.gyan404.ripple.service;

import com.gyan404.ripple.model.Transaction;

/***
 * Service interface for business logic abstractions
 */
public interface TrustLineService {
    boolean send(Transaction transaction);
    boolean receive(Transaction transaction);
    String getBalance();
}
