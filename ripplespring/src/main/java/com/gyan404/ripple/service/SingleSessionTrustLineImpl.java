package com.gyan404.ripple.service;

import com.gyan404.ripple.model.Amount;
import com.gyan404.ripple.model.Transaction;
import com.gyan404.ripple.network.NetworkManager;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class SingleSessionTrustLineImpl implements TrustLineService {

    /***
     * LOGGER.warn is used to bypass the spring INFO logs
     */
    private static Logger LOGGER = LoggerFactory.getLogger(SingleSessionTrustLineImpl.class);

    @Autowired
    NetworkManager networkManager;

    private Amount balance = new Amount(new AtomicInteger(0), "$");

    public boolean send(Transaction transaction) {
        if (transaction == null || transaction.getAmount() == null
                || transaction.getAmount().getBalance() == null
                || transaction.getAmount().getBalance().get() <= 0
                || TextUtils.isEmpty(transaction.getTargetUser())) {
            LOGGER.debug("Returning false from send()");
            return false;
        }
        LOGGER.warn("Paying " + transaction.getAmount().toString() + " to " + transaction.getTargetUser()+"...");
        if (networkManager.callTarget(transaction)) {
            balance.subtract(transaction.getAmount().getBalance().get());
            LOGGER.info("Sent");
        }
        LOGGER.warn("Trustline balance is: "+getBalance());
        LOGGER.debug("Returning true from send()");
        return true;
    }

    public boolean receive(Transaction transaction) {
        LOGGER.warn("You were paid " + transaction.getAmount().toString() + "!");
        balance.add(transaction.getAmount().getBalance().get());
        LOGGER.warn("Trustline balance is: "+getBalance());
        LOGGER.debug("Returning true from receive()");
        return true;
    }

    public String getBalance() {
        LOGGER.debug("Returning true from getBalance()");
        return balance.toString();
    }
}
