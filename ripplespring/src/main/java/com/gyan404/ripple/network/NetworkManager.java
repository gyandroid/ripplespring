package com.gyan404.ripple.network;

import com.gyan404.ripple.model.Amount;
import com.gyan404.ripple.model.Transaction;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class NetworkManager {

    private static Logger LOGGER = LoggerFactory.getLogger(NetworkManager.class);

    @Autowired
    private Target target;

    public boolean callTarget(Transaction transaction) {
        int statusCode = -1;
        Amount amount = transaction.getAmount();
        String user = transaction.getTargetUser();
        CloseableHttpClient client = HttpClients.createDefault();
        String url = target.url+target.port+"/trustline/"+"receive/"+amount.getBalance().get()+"/"+user;
        LOGGER.debug("URL: "+ url);
        HttpPost req = new HttpPost(url);
        try(CloseableHttpResponse response = client.execute(req)) {
            statusCode = response.getStatusLine().getStatusCode();
        } catch (IOException e) {
            LOGGER.error("Can't connect to "+ user);
            LOGGER.error(e.getMessage());
        }
        LOGGER.debug("statusCode: "+ statusCode);
        return statusCode == 200;
    }
}
