package com.gyan404.ripple.network;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Target {

    @Value("${target.url}")
    String url;

    @Value("${target.port}")
    String port;
}
