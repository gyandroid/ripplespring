package com.gyan404.ripple.controller;

import com.gyan404.ripple.model.Amount;
import com.gyan404.ripple.model.Transaction;
import com.gyan404.ripple.service.TrustLineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

/***
 * A RestController class to handle ReST API calls
 */
@RestController
public class Controller {

    private static Logger LOGGER = LoggerFactory.getLogger(Controller.class);


    @Autowired
    private TrustLineService trustLineService;

    @RequestMapping(method = RequestMethod.POST, value = "/send/{amount}/{user}")
    public @ResponseBody
    boolean send(@PathVariable("amount") int amount,
                 @PathVariable("user") String user) {
        LOGGER.debug("send()");
        return trustLineService.send(new Transaction(user,
                new Amount(new AtomicInteger(amount), "$")));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/receive/{amount}/{user}")
    public boolean receive(@PathVariable("amount") int amount,
                           @PathVariable("user") String user) {
        LOGGER.debug("receive()");
        return trustLineService.receive(new Transaction(user,
                new Amount(new AtomicInteger(amount), "$")));
    }

    @RequestMapping("/balance")
    public String getBalance() {
        LOGGER.debug("getBalance()");
        return trustLineService.getBalance();
    }
}