# TRUSTLINE APPLICATION

        =============================

           Welcome to the Trustline
           Trustline balance is: 0

        =============================

Application to transfer money between 2 parties, Alice and Bob

### Usage

1.Build the project

`mvn clean install`

2.Run the servers

Run `./alice.sh` and `./bob.sh` in different terminal/console windows.

3.Initiate transfer

Once the server has started from the previous step, use `./tl.sh` to initiate transfers

e.g. `./tl.sh alice 40 bob` will transfer $40 from alice to bob

`$ ./tl.sh alice 289 bob`
`alice paid 289 to bob`

and the 2 servers will show their respective balances as **WARN** log statements

### Frameworks/Libraries used

1. Spring Boot
2. Java 8
3. Embedded Jetty

### Test

1.`com.gyan404.ripple.HttpRequestTest` under `src/test/java/` is to test the balance method.

2.`com.gyan404.ripple.SmokeTest` is to test Controller autowiring.

### Notes

1.Amount.currency is only a cosmetic convenience for now. 
Can be used for currency conversion extensions.

2.Transaction is merely a syntactic sugar too.